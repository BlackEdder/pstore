import std.json : JSONValue;

string storeFile() {
    import standardpaths;
    import std.path : buildPath;
    return writablePath(StandardPath.data, "pstore", FolderFlag.create).
        buildPath("store.json");
}

string[string] extractKeyValue(string[] args) {
    string[string] kv;
    import std.range : empty, popFront, front;
    if (!args.empty)
        args.popFront;
    while(!args.empty) {
        import std.regex : regex, matchFirst;
        auto m = args.front.matchFirst(regex("^(.*?):(.*)$"));
        if (!m.empty) {
            kv[m.captures[1]] = m.captures[2];
        }
        args.popFront;
    }
    return kv;
}

JSONValue addKeyValue(JSONValue json, string key, string value) {
    // Convert value type
    import std.json : JSONException, parseJSON;
    try {
        auto v = value.parseJSON;
        json[key] = v;
    } catch (JSONException e) {
        json[key] = value;
    }
    return json;
}

void main(string[] args)
{
    import std.file : append;
    JSONValue json;

    auto keyValues = args.extractKeyValue;
    if (keyValues.length == 0) {
        import std.stdio : writeln;
        "Usage: pstore KEY1:VALUE1 KEY2:VALUE2 ...".writeln;
        return;
    }
    foreach(k, v; keyValues) {
        json = json.addKeyValue(k, v);
    }
    import std.datetime.systime : Clock;
    json["timestamp"] = Clock.currTime.toISOString;

    storeFile.append(json.toString ~ "\n");

    // TODO add support for pstore --filter (key) and --export csv
    // Also support taskwarrior like filters key:value
}
